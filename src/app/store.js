import {createStore,combineReducers} from 'redux';
import TaskEvent from '../components/task/taskEvent';

const appReducers=combineReducers({
    taskReducer:TaskEvent
});

const store=createStore(
    appReducers,
    undefined,
    undefined
)

export default store;