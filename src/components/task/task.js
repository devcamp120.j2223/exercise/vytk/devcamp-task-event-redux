import { Grid,TextField,Button } from "@mui/material";
import { Container } from "@mui/system";
import { useSelector,useDispatch } from "react-redux";

function Task (){
    const dispatch=useDispatch();

    const {taskList,taskNameInput}= useSelector((reduxData)=>reduxData.taskReducer);
    console.log(taskList);
    console.log(taskNameInput);

    const inputChangeHandler=(event)=>{
        console.log(event.target.value);
        dispatch({
            type:'TASK_INPUT_CHANGE',
            value:event.target.value
        })
    }
    const addTaskList=()=>{
        dispatch({
            type:'ADD_TASK'
        })
    }
    const changeTaskStatus=(index)=>{
        dispatch({
            type:'CHANGE_TASK_STATUS',
            index:index
        })
    }
    return(
        <Container>
            <Grid container mt={3}>
                <Grid xs={9} md={9} sm={12}  item>
                    <TextField fullWidth placeholder="Input task name" variant="outlined" label='Input Task name' value={taskNameInput} onChange={inputChangeHandler}/>
                </Grid>
                <Grid xs={3} md={3} lg={3} sm={12} item textAlign={'center'}>
                    <Button variant='contained' onClick={addTaskList}>Add Task</Button>
                </Grid>
            </Grid>
            <ol>
                {taskList.map((task,index)=>{
                    return <li key={index} style={{color:task.status?'green':'red'}} onClick={()=>changeTaskStatus(index)}>{task.taskName}</li>
                })}
            </ol>
        </Container>
    )
}

export default Task;